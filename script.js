var app=angular.module('myModule',[]);

app.controller('myController', function($scope, $rootScope, $location, $window) {
	var vm = this;
	vm.viewgrid = false;
  	vm.designationList = ["UI developer", "Full stack developer", "Android developer"];
  	vm.periodList = ["Within 15 days", "1 month", "2 month", "3 month", "More than 3 month"];
	
	vm.onSubmit = function(data){
		if(data) {
			var src = document.getElementById("attachment");
       		vm.applicant.cv = src.files[0];			
			
			vm.viewgrid = true;		
		}
	}
	
	vm.show_data = function(){
		console.log(vm.applicant);
	}
	
	vm.backToApplication = function(){
		vm.applicant = {};
		vm.viewgrid = false;	
	}
});

